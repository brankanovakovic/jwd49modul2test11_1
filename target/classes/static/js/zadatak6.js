//cist javascript
function validacija() {
	
	var registracijaInput = document.getElementById("registracija")
	var pocetakParkingaInput = document.getElementById("pocetakParkinga")
	var trajanjeUMinutimaInput = document.getElementById("trajanjeUMinutima")
	var zonaInput = document.getElementById("zona")
	var daInput = document.getElementById("DA")
	var neInput = document.getElementById("NE")
	
	var registracija = registracijaInput.value
	var pocetakParkinga = pocetakParkingaInput.value
	var trajanjeUMinutima = trajanjeUMinutimaInput.value
	var zona = zonaInput.value
	
	var greska = false;
	if (registracija == "") {
		greska = true;
		alert("Popunite polje registracija.")
	}

	if (pocetakParkinga == "") {
		greska = true;
		alert("Popunite polje pocetak parkinga.")
	}
	if (trajanjeUMinutima == "" || trajanjeUMinutima <= 15 || trajanjeUMinutima.match("^[0-9]+$") == null) {
		greska = true;
		alert("Greska prilikom unosa polja trajanje parkinga.")
	}

	if (zona === "crvena" && trajanjeUMinutima > 120) {
		greska = true;
		alert("Dozvoljeno vreme parkiranja u crvenoj zoni je 120 minuta.")
	}

	if (daInput.checked == false && neInput.checked == false) {
		greska = true;
		alert("Osoba sa invaliditetom? Da? Ne?")
	}

	if (greska === true) {
		event.preventDefault()

	}
}