var baseURL = ""

function popuniBaseURL() {
	// traži od servera baseURL
	$.get("Zone/baseURL", function(odgovor) { // GET zahtev
		console.log(odgovor)

		if (odgovor.status == "ok") {
			baseURL = odgovor.baseURL // inicjalizuj globalnu promenljivu baseURL
			$("base").attr("href", baseURL) // postavi href atribut base elementa
		}
	})
	console.log("GET: " + "baseURL")
}

function popuniZone() {
	var tabela = $("table.tabela")
	var nazivInput = $("input[name=naziv]")
	var cenaZaSatInput =  $("input[name=cenaZaSat]")
	var dozvoljenoVremeParkingaUSatimaInput =  $("input[name=dozvoljenoVremeParkingaUSatima]")
	var naziv = nazivInput.val()
	var cenaZaSat = cenaZaSatInput.val()
	var dozvoljenoVremeParkingaUSatima= dozvoljenoVremeParkingaUSatimaInput.val()
	
	var params = {
		naziv: naziv,
		cenaZaSat: cenaZaSat,
		dozvoljenoVremeParkingaUSatima: dozvoljenoVremeParkingaUSatima
	}
	console.log(params)
	$.get("Zone", params, function(odgovor) {
		console.log(odgovor)

		if (odgovor.status == "ok") {
			tabela.find("tr:gt(1)").remove()
			
			var zone = odgovor.zone
			for (var it in zone) {
				tabela.append(
					'<tr>' + 
						'<td>' + zone[it].naziv + '</td>' + 
						'<td>' + zone[it].cenaZaSat + '</td>' + 
						'<td>' + zone[it].cenaZaSat*1.18 + '</td>' + 
						'<td>' + zone[it].dozvoljenoVremeParkingaUSatima + '</td>' + 
						
					'</tr>'
				)
			}
		}
	})
	console.log("GET: Zone")
}


function dodajZona() {

	// kupimo parametre iz forme u varijable / promenljive
	var nazivInput = $("input[name=naziv]")
	var cenaZaSatInput =  $("input[name=cenaZaSat]")
	var dozvoljenoVremeParkingaUSatimaInput =  $("input[name=dozvoljenoVremeParkingaUSatima]")
	var naziv = nazivInput.val()
	var cenaZaSat = cenaZaSatInput.val()
	var dozvoljenoVremeParkingaUSatima= dozvoljenoVremeParkingaUSatimaInput.val()
	
	// pravimo JSON objekat od pokupljenih podataka
	var params = {
		naziv: naziv,
		cenaZaSat: cenaZaSat,
		dozvoljenoVremeParkingaUSatima: dozvoljenoVremeParkingaUSatima
	}
	console.log(params)
	
	// pravimo / ispucavate POST request ka serveru ( odgovarajucem kontroleru)
		
	$.post("Zone/Create", params, function(odgovor) {
			
		// telo od funkcije - function(odgovor) - prestavlja funkciju koja se izvrsi tek kad dobijemo odgovor od servera
		// ovde racunaj cenu za zadatak7.html /Parking
		console.log(odgovor)
	window.location.replace("zadatak7.html")
		if (odgovor.status == "ok") {
		
		// } else if (odgovor.status == "neovlascen") {
			// window.location.replace("zanrovi.html")
		} else if (odgovor.status == "greska") {
			$("p.greska").text(odgovor.poruka)
		}
	})
	console.log("POST: Zone/Create")
}

$(document).ready(function() {
	popuniBaseURL() 
	popuniZone()
	$("form").submit(function() {
		dodajZona()
	return false

})
})
