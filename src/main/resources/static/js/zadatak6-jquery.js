// Document is ready 
$(document).ready(function() {


	let registracijaGreska = true;
	$('#registracija').keyup(function() {
		validateRegistracija();
	});

	function validateRegistracija() {

		let test = $('input[name=registracija]');

		let registracijaValue = $('input[name=registracija]').val();
		if (registracijaValue.length == 0) {
			registracijaGreska = false;
			alert("Popunite polje registracija.")
			return false;
		}
	}

	let pocetakParkingaGreska = true;
	$('#pocetakParkinga').keyup(function() {
		validatePocetakParkinga();
	});

	function validatePocetakParkinga() {
		let pocetakParkingaValue = $('#pocetakParkinga').val();
		if (pocetakParkingaValue.length == 0) {
			pocetakParkingaGreska = false;
			alert("Popunite polje pocetak parkinga.")
			return false;
		}
	}

	let trajanjeUMinutimaGreska = true;
	$('#trajanjeUMinutima').keyup(function() {
		validateTrajanjeUMinutima();
	});

	function validateTrajanjeUMinutima() {
		let trajanjeUMinutimaValue = $('#trajanjeUMinutima').val();
		if (trajanjeUMinutimaValue.length == 0 || trajanjeUMinutimaValue <= 15 || trajanjeUMinutimaValue.match("^[0-9]+$") == null ) {
			alert("Greska prilikom unosa polja trajanje parkinga.");
			trajanjeUMinutimaGreska = false;
			return false;
		}
	}

	let osobaSaInvaliditetomGreska = true;
	$('input[name=osobaSaInvaliditetom]').keyup(function() {
		validateOsobaSaInvaliditetom();
	});

	function validateOsobaSaInvaliditetom() {
		let izborKategorije = $('input[name=osobaSaInvaliditetom]');
		let da = izborKategorije[0].checked
		let ne = izborKategorije[1].checked
		if (da == false && ne == false) {
			osobaSaInvaliditetomGreska = false;
			alert("Osoba sa invaliditetom? Da? Ne?")
			return false;
		}
	}

	let zonaGreska = true;
	$('#zona').keyup(function() {
		validateZona();
	});

	function validateZona() {
		let zonaValue = $('#zona').val();
		if (zonaValue.length == 0 || zonaValue === "crvena" && trajanjeUMinutimaValue > 120) {
			zonaGreska = false;
			alert("Dozvoljeno vreme parkiranja u crvenoj zoni je 120 minuta.")
			return false;
		}
	}

	// Submitt button 
	$('#submit').click(function() {
		validateRegistracija();
		validatePocetakParkinga();
		validateTrajanjeUMinutima();
		validateOsobaSaInvaliditetom();
		validateZona
		if ((registracijaGreska == true) ||
			(pocetakParkingaGreska == true) ||
			(trajanjeUMinutimaGreska == true) ||
			(osobaSaInvaliditetomGreska == true) ||
			(zonaGreska == true)) {
			return false;
		} else {
			return true;
		}
	});
}); 