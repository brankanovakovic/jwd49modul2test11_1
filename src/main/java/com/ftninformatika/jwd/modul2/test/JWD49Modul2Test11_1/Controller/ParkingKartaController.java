package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.Controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.OsobaSaInvaliditetom;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.Zona;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.ParkingKartaService;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.ZonaService;

@Controller
//@RequestMapping(value="")
public class ParkingKartaController implements ServletContextAware {


	//OVDE INICIJALIZUJES MAPE ZA MEMORIJU NA NIVOU APLIKACIJE:
	private Map<Integer, ParkingKarta> parkingKarte= new HashMap<Integer, ParkingKarta>();
	private Map<Integer, Zona> zone= new HashMap<Integer, Zona>();
	//4.ti ZADATAK DODAJES MAPU ZA PARKING KARTE KORISNIKA TJ KARTE KOJE JE DODAO KORISNIK U SESIJI
	private Map<Integer, ParkingKarta> parkingKarteKorisnikaIzSesije = new HashMap();

	//OVDE INICIJALIZUJES BROJAC ZA CRVENE KARTE I ID
	private int brojKarataUCrvenojZoni=0;
	
	private int id=1;
	
	//4.ti ZADATAK JER TREBAMO DA DODAMO KARTE U KORISNIKA IZ SESIJE TJ KARTE KOJE SU NAKNADNO DODATE
	private static final String PARKING_KARTE= "PARKING_KARTE";

	@Autowired
	private ParkingKartaService parkingKartaService;

	@Autowired
	private ZonaService zonaService;
	
	
	@Autowired
	private ServletContext servletContext;
	
	private String baseURL;

	//	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	///////////// 	ZADATAK 1 ////////////////////
	// KREIRAS KLASE ZONA I PARKING KARTA I KONTROLER ZA PARKING KARTU
	// U KONTROLERU PARKING KARTA NAPRAVIS INIT METODU GDE CES NAPRAVITI INTERNU MEMORIJU TJ MAPU PARKING KARATA
	// PRAVIS I MAPU ZONA JER U PARKING KARTI IMAS OBJEKAT ZONU


	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath();

		zone.put(1,new Zona(1, "Crvena", 53.0, 2));
		zone.put(2,new Zona(2, "Plava", 44.0, 12));
		zone.put(3,new Zona(3, "Bela", 33.0, 24));

		parkingKarte.put(id,new ParkingKarta(id,"NS111NS", "2019-12-18 11:30:00", 60, OsobaSaInvaliditetom.NE, zone.get(1)));
		//ID SI GENERISALA DA JE 1 I UBACILA U PRVU PARKING KARTU, SADA GA POVECAS SA ++ ZA SLEDECU PARKING KARTU I TAKO DALJE
		id++;
		parkingKarte.put(id,new ParkingKarta(id,"NS222NS", "2020-12-18 11:30:00", 60, OsobaSaInvaliditetom.DA, zone.get(2)));
		id++;
		parkingKarte.put(id,new ParkingKarta(id,"NS333NS", "2021-12-18 11:30:00", 60, OsobaSaInvaliditetom.NE, zone.get(3)));
		id++;
		// ovde sam uvecala brojac za 1 zato sto smo u mapi stavili da imamo jednu kartu u crvenoj zoni
		brojKarataUCrvenojZoni++;

	}

	////////////// ZADATAK 2 //////////////////
	//PRVO SI KOPIRALA SABLON U STATIC, ZATIM PREIMENOVALA U ZADATAK2.HTML ZATIM SI OSTAVILA SAMO FORM A TABELU IZ SABLONA OBRISALA

	//OVIM GETMAPPING ZADATAK2.HTML U STVARI POKRECES HTML PREKO JAVE, TJ U URL KADA UKUCAS http://localhost:8080/Parking/zadatak2.html OTVARAS OVAJ HTML
	//TJ DOBIJAS FORMU
	// SA GET ZAHTEVOM DOBIJAS PODATKE SA SERVERA (KORISIS GET I ZA PRETRAGE KADA SALJES NEKE PARAMETRE KAO KRITERIJUME PRETRAGE
	// POST ZAHTEV KORISTIS KADA HOCES DA MENJAS NESTO NA SERVERU ZNACI ZA DODAVANJE, IZMENE, BRISANJE

	@GetMapping(value="/Parking/zadatak2.html")
	public String getParkingRequestForm(Model model) {
		//		 model.addAttribute("parkingKarta", new ParkingKarta()); //ZA SADA NE TREBA
		return "zadatak2";
	}
	//OVDE KREIRAS HANDLER METODU KOJA OBRADJUJE POST ZAHTEVE ZA KREIRANJE PARKING KARTE
	//PREUZIMA PODATKE IZ FORME TO SU TI REQUESTPARAM ALI!!! MORAS U ZADATAK2.HTML U SVAKOM INPUT POLJU DA 
	//NAPISES NAME="" I DA NAPISES KOJE JE POLJE KAKO BI POVEZALA FORMU I OVU METODU ZNACI PISES ZA SVAKO POLJE
	//NAME="registracija" ... NAME="pocetakParkinga"...
	//PostMapping createParking karta je u 2. zadatku bio void, u trecem izmenjeno na string
	@PostMapping(value="/ParkingKarte")
	public String CreateParkingKarta(
			@RequestParam String registracija, 
			@RequestParam String pocetakParkinga,
			@RequestParam int trajanjeUMinutima,
			@RequestParam OsobaSaInvaliditetom osobaSaInvaliditetom,
			@RequestParam String nazivZona,
			HttpServletRequest request) throws IOException {
		//OVDE PRONALAZIS PRVO ZONU IZ POSTOJECE KOLEKCIJE, zona je ZONA KOJU HOCES DA PRONADJES I PROLAZIS KROZ FOR PETLJU KROZ SVAKU ZONU
		//AKO POSTOJI ZONA KOJU SI UKUCALA U INPUT POLJE (nazivZona) ONDA CE zona PREUZETI NJENU REFERENCU
		Zona zona = null;	
		for(Zona itZona : zone.values()) {
			if(itZona.getNaziv().equalsIgnoreCase(nazivZona)) {
				zona = itZona;
				break;
			}
		}
		//KAO STO U ZADATKU KAZE KREIRAS NOVU PARKING KARTU
		ParkingKarta parkingKarta = new ParkingKarta(id, registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zona);
		//POSTO PODATKE CUVAS U MEMORIJI APLIKACIJE MORAS DA DODAS NOVU PARKING KARTU KOJU SI KREIRALA U MAPU:
		parkingKarte.put(id, parkingKarta);
		id++;

		//TRAZE ISPIS U KONZOLI ECLIPS-A I TO JE SLEDECE:
		System.out.println("");
		System.out.println("ISPIS U KONZOLI ZADATAK 3");
		System.out.println();
		for(ParkingKarta pk : parkingKarte.values()) {
			System.out.println(pk);
		}
		//TAKODJE TRAZE DA SE U MEMORIJI CUVA UKUPAN BROJ CRVENIH KARATA I TI SI VEC KREIRALA GORE BROJAC CRVENIH KARATA
		// I SADA SAMO PROVERIS DA LI JE KARTA CRVENA AKO JESTE POVECAS BROJAC ZA JEDAN
		System.out.println("");
		System.out.println("ISPIS U KONZOLI ZADATAK 3");
		System.out.println();
		if(nazivZona.equalsIgnoreCase("Crvena")) {
			brojKarataUCrvenojZoni++;
		}
		//TRAZE ISPIS BROJA CRVENIH KARATA U ECLIPS KONZOLI:
		System.out.println("Broj karata u crvenoj zoni je: " + brojKarataUCrvenojZoni + ".");

						///////////////////// ZADATAK 4 /////////////////////////
		parkingKarteKorisnikaIzSesije = (HashMap<Integer, ParkingKarta>) request.getSession().getAttribute("PARKING_KARTE");
		if (parkingKarteKorisnikaIzSesije == null) {
			parkingKarteKorisnikaIzSesije = new HashMap<Integer, ParkingKarta>();
		}
		parkingKarteKorisnikaIzSesije.put(parkingKarta.getId(), parkingKarta);
		request.getSession().setAttribute(PARKING_KARTE, parkingKarteKorisnikaIzSesije);
						///////////////////// KRAJ ZADATAK 4 /////////////////////////

		return "redirect:/ParkingKarte/Zadatak3";
	}

	///////////////   ZADATAK 3    /////////////////
	/// SHODNO TOME DA TI TREBA SAMO PRIKAZ I NE MENJAS NISTA NA SERVERU (NEMAS IZMENA, DODAVANJE, BRISANJE) KORISTIS GET METODU
	// HTTP REQUEST DODAJEMO KAO PARAMETAR MODELANDVIEW TEK U 4TOM ZADATKU-CITAMO IZ SESIJE
	@GetMapping(value="/ParkingKarte/Zadatak3")
	public String Zadatak3(
			HttpSession session,
			HttpServletRequest request,
			Model model) throws IOException {
		//parkingKarte je naziv modela koji koristim u template-u pogledaj zadatak3.html to je ${parkingKarte}
		// a parkingKarte.values() je mapa koju prosledjujemo kao objekat
		model.addAttribute("parkingKarte", parkingKarte.values());

		double ukupnoZaradjenaSuma = 0.0;
		
		//npr trajanje parkinga u minutima je 150minuta trajanjeUSatima je onda 2h i preostaloTrajanjeUMinutima je 30min
		//u sledecoj for petlji posle if ne treba else jer je double ukupne zarade postavljen vec na 0
		int trajanjeUSatima = 0;
		int preostaloTrajanjeUMinutima = 0;
		for(ParkingKarta pk : parkingKarte.values()) {
			if(pk.getOsobaSaInvaliditetom().equals(OsobaSaInvaliditetom.NE)) {
				trajanjeUSatima = pk.getTrajanjeUMinutima() / 60;
				preostaloTrajanjeUMinutima = pk.getTrajanjeUMinutima() % 60;
				if(preostaloTrajanjeUMinutima > 0) {
					trajanjeUSatima++;
				}
				ukupnoZaradjenaSuma += trajanjeUSatima * pk.getZona().getCenaZaSat();
			}
		}
		//u model ubacujem i ukupnoZaradjenuSumu i naziv "ukupnoZaradjenaSuma" i u zadatak3.html mora isto tako da se zove linija koda 26
		model.addAttribute("ukupnoZaradjenaSuma", ukupnoZaradjenaSuma);

				/////////////////// ZADATAK 4 ///////////////////
//		//CITAMO IZ SESIJE TAKO STO:
		parkingKarteKorisnikaIzSesije = (HashMap<Integer,ParkingKarta>)request.getSession().getAttribute(PARKING_KARTE);
		//provera da li imamo neke karte korisnika sacuvane u sesiji mora biti posle vadjenja vrednosti iz sesije u slucaju
		//da korisnik nije napravio ni jednu parking kartu program bi pukao jer bi podaci iz sessije bili null
		//ovom proverom postavljam listu karata korisnika iz sesije na praznu hashmapu
		if(parkingKarteKorisnikaIzSesije==null) {
			parkingKarteKorisnikaIzSesije = new HashMap();
		}
		//ISPISEMO U KONZOLI KOJE SU KARTE KOJE JE KORISNIK IZ SESIJE NAPRAVIO:
		System.out.println("");
		System.out.println("ISPIS U KONZOLI ZADATAK 4");
		System.out.println();
		for(ParkingKarta pk : parkingKarteKorisnikaIzSesije.values()) {
			System.out.println(pk);
		}
				/////////////////// KRAJ ZADATAK 4 ///////////////////
		//vracam zadatak3.html
		return "zadatak3";
	}
	////////////// ZADATAK 5 ///////////////
	
	//PARAMETRI REQUESTPARAM trajanjeUMinutimaOd i trajanjeUMinutimaDo mora da se poklapa sa name input polja u zadatak5.html pogledaj html
	@GetMapping(value="/ParkingKarte/Zadatak5")
	public ModelAndView Zadatak5(
			@RequestParam(required=false, defaultValue="0") Integer trajanjeUMinutimaOd,
			@RequestParam(required=false, defaultValue="160") Integer trajanjeUMinutimaDo) throws IOException {
		//ovde preuzimamo listu parking karata ali iz baze, prvo se poziva servis, pa baza.
		List<ParkingKarta> parkingKarte = parkingKartaService.getParkingKartaByTrajanjeUMinutimaOdAndTrajanjeUMinutimaDo(trajanjeUMinutimaOd, trajanjeUMinutimaDo);
		//da li je potrebna lista zona:
		List<Zona> zone = zonaService.getAll();
		//modelandview ubacujemo prvo zadatak5.html
		ModelAndView rezultat = new ModelAndView("zadatak5");
		//zatim ubacujemo objekte, prvi objekat koji ubacujemo je objekat parking karte koje smo izvukli iz baze i dodeljujemo naziv
		//"ParkingKarte" ovo mora da se poklapa sa nazivom u zadatku5.html ${ParkingKarte}
		rezultat.addObject("ParkingKarte", parkingKarte);
		//drugi objekat koji prosledjujemo su zone, mada nisam sigurna da nam je ovo potrebno
		rezultat.addObject("zone", zone);

		double ukupnoZaradjenaSuma = 0.0;
		
		//npr trajanje parkinga u minutima je 150minuta trajanjeUSatima je onda 2h i preostaloTrajanjeUMinutima je 30min
		//u sledecoj for petlji posle if ne treba else jer je double ukupne zarade postavljen vec na 0
		int trajanjeUSatima = 0;
		int preostaloTrajanjeUMinutima = 0;
		System.out.println("");
		System.out.println("ISPIS U KONZOLI ZADATAK 5");
		System.out.println();
		for(ParkingKarta pk : parkingKarte) {
			if(pk.getOsobaSaInvaliditetom().equals(OsobaSaInvaliditetom.NE)) {
				trajanjeUSatima = pk.getTrajanjeUMinutima() / 60;
				preostaloTrajanjeUMinutima = pk.getTrajanjeUMinutima() % 60;
				if(preostaloTrajanjeUMinutima > 0) {
					trajanjeUSatima++;
				}
				ukupnoZaradjenaSuma += trajanjeUSatima * pk.getZona().getCenaZaSat();
			}
			System.out.println(pk);
		}
		//u modelandview jos zaradu, ovaj "ukupnoZaradjenaSuma" mora da se poklapa sa zadatak5.html "${ukupnoZaradjenaSuma}" (pogledaj html)
		rezultat.addObject("ukupnoZaradjenaSuma", ukupnoZaradjenaSuma);
		//i vratis modelandview
		return rezultat;
	}

}