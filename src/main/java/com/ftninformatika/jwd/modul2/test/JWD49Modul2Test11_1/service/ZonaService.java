package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service;

import java.util.List;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.Zona;

public interface ZonaService {

	List<Zona> getAll();

	int save(Zona zona);

}
