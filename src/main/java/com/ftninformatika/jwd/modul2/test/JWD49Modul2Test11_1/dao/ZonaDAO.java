package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.Zona;

@Component
public class ZonaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static class ZonaRowMapper implements RowMapper<Zona> {
		public Zona mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String naziv = rs.getString(index++);
			double cenaZaSat = rs.getDouble(index++);
			int dozvoljenoVremeParkingaUSatima = rs.getInt(index++);

			Zona zona = new Zona(id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima);
			return zona;
		}

	}
	public List<Zona> findAll() {
		String sql = "SELECT id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima FROM zone";
		return jdbcTemplate.query(sql, new ZonaRowMapper());
	}
	public int save(Zona zona) {
		String sql = "INSERT INTO zone (naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima) VALUES (?, ?, ?)";
		return jdbcTemplate.update(sql, zona.getNaziv(), zona.getCenaZaSat(), zona.getDozvoljenoVremeParkingaUSatima());
	}
	
}
