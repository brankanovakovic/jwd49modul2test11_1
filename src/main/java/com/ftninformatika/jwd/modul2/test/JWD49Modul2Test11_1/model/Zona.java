package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model;

public class Zona {
	
 private int id;  
 private String naziv;
 private double cenaZaSat;
 private int dozvoljenoVremeParkingaUSatima;
public Zona(int id, String naziv, double cenaZaSat, int dozvoljenoVremeParkingaUSatima) {
	super();
	this.id = id;
	this.naziv = naziv;
	this.cenaZaSat = cenaZaSat;
	this.dozvoljenoVremeParkingaUSatima = dozvoljenoVremeParkingaUSatima;
}

public Zona(String naziv, double cenaZaSat, int dozvoljenoVremeParkingaUSatima) {
	super();
	this.naziv = naziv;
	this.cenaZaSat = cenaZaSat;
	this.dozvoljenoVremeParkingaUSatima = dozvoljenoVremeParkingaUSatima;
}

public Zona() {
	super();
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getNaziv() {
	return naziv;
}
public void setNaziv(String naziv) {
	this.naziv = naziv;
}
public double getCenaZaSat() {
	return cenaZaSat;
}
public void setCenaZaSat(double cenaZaSat) {
	this.cenaZaSat = cenaZaSat;
}
public int getDozvoljenoVremeParkingaUSatima() {
	return dozvoljenoVremeParkingaUSatima;
}
public void setDozvoljenoVremeParkingaUSatima(int dozvoljenoVremeParkingaUSatima) {
	this.dozvoljenoVremeParkingaUSatima = dozvoljenoVremeParkingaUSatima;
}
@Override
public String toString() {
	return "Zona [id=" + id + ", naziv=" + naziv + ", cenaZaSat=" + cenaZaSat + ", dozvoljenoVremeParkingaUSatima="
			+ dozvoljenoVremeParkingaUSatima + "]";
}
	
 

}
