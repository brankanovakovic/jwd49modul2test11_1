package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service;

import java.util.List;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.ParkingKarta;

public interface ParkingKartaService {

	List<ParkingKarta> getParkingKartaByTrajanjeUMinutimaOdAndTrajanjeUMinutimaDo
	(Integer trajanjeUMinutimaOd, Integer trajanjeUMinutimaDo);


}
