package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model;

public class ParkingKarta {
	private int id;
	private String registracija;
	private String pocetakParkinga;
	private int trajanjeUMinutima;
	private OsobaSaInvaliditetom osobaSaInvaliditetom;
	private Zona zona;
	
	public ParkingKarta() {
	}

	public ParkingKarta(int id, String registracija, String pocetakParkinga, int trajanjeUMinutima,
			OsobaSaInvaliditetom osobaSaInvaliditetom, Zona zona) {
		this.id = id;
		this.registracija = registracija;
		this.pocetakParkinga = pocetakParkinga;
		this.trajanjeUMinutima = trajanjeUMinutima;
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
		this.zona = zona;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRegistracija() {
		return registracija;
	}

	public void setRegistracija(String registracija) {
		this.registracija = registracija;
	}

	public String getPocetakParkinga() {
		return pocetakParkinga;
	}

	public void setPocetakParkinga(String pocetakParkinga) {
		this.pocetakParkinga = pocetakParkinga;
	}

	public int getTrajanjeUMinutima() {
		return trajanjeUMinutima;
	}

	public void setTrajanjeUMinutima(int trajanjeUMinutima) {
		this.trajanjeUMinutima = trajanjeUMinutima;
	}

	public OsobaSaInvaliditetom getOsobaSaInvaliditetom() {
		return osobaSaInvaliditetom;
	}

	public void setOsobaSaInvaliditetom(OsobaSaInvaliditetom osobaSaInvaliditetom) {
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	@Override
	public String toString() {
		return "ParkingKarta [id=" + id + ", registracija=" + registracija + ", pocetakParkinga=" + pocetakParkinga
				+ ", trajanjeUMinutima=" + trajanjeUMinutima + ", osobaSaInvaliditetom=" + osobaSaInvaliditetom
				+ ", zona=" + zona + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((osobaSaInvaliditetom == null) ? 0 : osobaSaInvaliditetom.hashCode());
		result = prime * result + ((pocetakParkinga == null) ? 0 : pocetakParkinga.hashCode());
		result = prime * result + ((registracija == null) ? 0 : registracija.hashCode());
		result = prime * result + trajanjeUMinutima;
		result = prime * result + ((zona == null) ? 0 : zona.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParkingKarta other = (ParkingKarta) obj;
		if (id != other.id)
			return false;
		if (osobaSaInvaliditetom != other.osobaSaInvaliditetom)
			return false;
		if (pocetakParkinga == null) {
			if (other.pocetakParkinga != null)
				return false;
		} else if (!pocetakParkinga.equals(other.pocetakParkinga))
			return false;
		if (registracija == null) {
			if (other.registracija != null)
				return false;
		} else if (!registracija.equals(other.registracija))
			return false;
		if (trajanjeUMinutima != other.trajanjeUMinutima)
			return false;
		if (zona == null) {
			if (other.zona != null)
				return false;
		} else if (!zona.equals(other.zona))
			return false;
		return true;
	}
	
}
