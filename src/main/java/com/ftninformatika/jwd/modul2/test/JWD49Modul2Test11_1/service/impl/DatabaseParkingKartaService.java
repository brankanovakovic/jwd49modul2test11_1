package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.dao.ParkingKartaDAO;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.ParkingKartaService;

@Service
public class DatabaseParkingKartaService implements ParkingKartaService{

	@Autowired
	private ParkingKartaDAO parkingKartaDAO;

	public List<ParkingKarta> getParkingKartaByTrajanjeUMinutimaOdAndTrajanjeUMinutimaDo(
			Integer trajanjeUMinutimaOd, Integer trajanjeUMinutimaDo) {
	if (trajanjeUMinutimaOd == null) {
		trajanjeUMinutimaOd = 0;
	}
	if (trajanjeUMinutimaDo == null) {
		trajanjeUMinutimaDo = Integer.MAX_VALUE;
	}
	return parkingKartaDAO.find(trajanjeUMinutimaOd, trajanjeUMinutimaDo);
	}
	
	

}
