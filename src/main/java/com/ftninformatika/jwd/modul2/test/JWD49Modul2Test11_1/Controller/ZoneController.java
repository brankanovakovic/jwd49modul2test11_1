package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.Controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.Zona;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.ZonaService;

@Controller
@RequestMapping(value="/Zone")
public class ZoneController {

	@Autowired
	private ZonaService zoneService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	@GetMapping
	@ResponseBody
	public Map<String, Object> index(@RequestParam(required=false, defaultValue="") String naziv,
			@RequestParam(required=false, defaultValue="") Double cenaZaSat,
			@RequestParam(required=false, defaultValue="") Integer dozvoljenoVremeParkingaUSatima) {
		// čitanje
		List<Zona> zone = zoneService.getAll();

		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("zone", zone);
		for(Zona z :zone) {
			System.out.println(z.toString());
		}
		return odgovor;
	}
	
	@GetMapping(value="baseURL")
	@ResponseBody
	public Map<String, Object> baseURL() {
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("baseURL", baseURL);	
		return odgovor;
	}
	@PostMapping(value="/Create")
	@ResponseBody
	public Map<String, Object> create(@RequestParam String naziv, 
			@RequestParam(required=false, defaultValue="") Double cenaZaSat,
			@RequestParam(required=false, defaultValue="") Integer dozvoljenoVremeParkingaUSatima,
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		
	
		// validacija
		if (naziv.equals("")) {
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", "Naziv ne sme biti prazan!");
			return odgovor;
		}

		Zona zona= new Zona(naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima);
		
		zoneService.save(zona);
		
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		return odgovor;
	}
	
}
