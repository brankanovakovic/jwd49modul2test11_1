package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.OsobaSaInvaliditetom;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.Zona;

@Component
public class ParkingKartaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static class ParkingKartaRowMapper implements RowMapper<ParkingKarta> {

		public ParkingKarta mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String registracija = rs.getString(index++);
			String pocetakParkinga = rs.getString(index++);
			int trajanjeUMinutima = rs.getInt(index++);
			String osobaSaInvaliditetom = rs.getString(index++);

			int zonaId = rs.getInt(index++);
			String zonaNaziv = rs.getString(index++);
			double cenaZaSat = rs.getDouble(index++);
			int dozvoljenoVremeParkingaUSatima = rs.getInt(index++);
			Zona zona = new Zona(zonaId, zonaNaziv, cenaZaSat, dozvoljenoVremeParkingaUSatima);
			
			OsobaSaInvaliditetom osi;
			if(osobaSaInvaliditetom.equalsIgnoreCase("DA")) {
				osi = OsobaSaInvaliditetom.DA;
			}else {
				osi = OsobaSaInvaliditetom.NE;
			}
			ParkingKarta parkingKarta = new ParkingKarta(id, registracija, pocetakParkinga, trajanjeUMinutima, osi, zona);
			return parkingKarta;
		}
	}
	public List<ParkingKarta> find(Integer trajanjeUMinutimaOd, Integer trajanjeUMinutimaDo) {
		String sql = 
				"SELECT p.id, p.registracija, p.pocetakParkinga, p.trajanjeUMinutima, p.osobaSaInvaliditetom, p.zonaId, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima FROM parkingKarte p \r\n"
				+ "LEFT JOIN zone z ON p.zonaId = z.id \r\n"
				+ "WHERE p.trajanjeUMinutima >= ? AND p.trajanjeUMinutima <= ?\r\n"
				+ "ORDER BY p.id";
		return jdbcTemplate.query(sql, new ParkingKartaRowMapper(), trajanjeUMinutimaOd, trajanjeUMinutimaDo);
	}
	
}
