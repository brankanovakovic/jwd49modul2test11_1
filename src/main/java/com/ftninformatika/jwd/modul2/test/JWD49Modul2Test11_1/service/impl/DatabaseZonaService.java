package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.dao.ZonaDAO;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.model.Zona;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test11_1.service.ZonaService;

@Service
public class DatabaseZonaService implements ZonaService{

	@Autowired
	private ZonaDAO zonaDAO;
	
	public List<Zona> getAll() {
		return zonaDAO.findAll();
	}

	public int save(Zona zona) {
		return zonaDAO.save(zona);
	}


}
